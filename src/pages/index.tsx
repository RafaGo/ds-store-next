import Head from 'next/head'
import styles from './home.module.scss'
import { SubscribeButton } from '@/components/SubscribeButton'
import { GetServerSideProps } from 'next'

export default function Home() {
  return (
    <>
      <Head>
        <title>ig.news</title>
      </Head>
      <main className={styles.contentContainer}>
        <section className={styles.hero}>
          <span>Hey, welcome</span>
          <h1>News about the <span>React</span></h1>
          <p>
            Get acess to all the publications <br />
            <span>for $9.90 </span>
          </p>
          <SubscribeButton />
        </section>
        <img src='/images/avatar.svg' alt='Girl coding' />
      </main>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    props: {
      nome: 'Rafael'

    }
  }
}